package com.florangoutang.empirestrikesback.injection

import com.florangoutang.empirestrikesback.ui.TripListFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Subcomponent(modules = [TripListFragmentModule::class])
interface TripListFragmentComponent : AndroidInjector<TripListFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<TripListFragment>()
}