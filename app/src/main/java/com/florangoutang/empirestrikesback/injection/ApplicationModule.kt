package com.florangoutang.empirestrikesback.injection

import android.app.Application
import android.arch.persistence.room.Room
import com.florangoutang.empirestrikesback.data.TripDataSourceImpl
import com.florangoutang.empirestrikesback.data.TripDatabase
import com.florangoutang.empirestrikesback.interfaceadapter.trip.common.StarNumberTransformer
import com.florangoutang.empirestrikesback.interfaceadapter.trip.common.TripRemoteTransformer
import com.florangoutang.empirestrikesback.interfaceadapter.util.scheduler.BaseSchedulerProvider
import com.florangoutang.empirestrikesback.interfaceadapter.util.scheduler.SchedulerProvider
import com.florangoutang.empirestrikesback.usecase.trip.TripDataSource
import com.florangoutang.empirestrikesback.usecase.trip.TripRepository
import com.florangoutang.empirestrikesback.usecase.trip.TripRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(subcomponents = [MainActivityComponent::class,
    TripListFragmentComponent::class,
    TripDetailFragmentComponent::class
])
class ApplicationModule {

    @Provides
    fun provideTripRepository(dataSource: TripDataSource): TripRepository {
        return TripRepositoryImpl(dataSource)
    }

    @Provides
    fun provideTripDataSource(listTransformer: TripRemoteTransformer,
                              database: TripDatabase): TripDataSource {
        return TripDataSourceImpl(listTransformer, database)
    }

    @Provides
    fun provideTripRemoteTransformer() = TripRemoteTransformer()

    @Provides
    fun provideStarNumberTransformer() = StarNumberTransformer()

    @Singleton
    @Provides
    fun provideTripDatabase(application: Application): TripDatabase {
        return Room.databaseBuilder(
                application.applicationContext,
                TripDatabase::class.java, "trip-database"
        ).build()
    }

    @Provides
    fun provideBaseSchedulerProvider() : BaseSchedulerProvider {
        return SchedulerProvider()
    }
}