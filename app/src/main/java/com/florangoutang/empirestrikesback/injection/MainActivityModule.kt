package com.florangoutang.empirestrikesback.injection

import com.florangoutang.empirestrikesback.ui.MainActivity
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {

    @Provides
    fun provideMainActivity(): MainActivity {
        return MainActivity()
    }
}