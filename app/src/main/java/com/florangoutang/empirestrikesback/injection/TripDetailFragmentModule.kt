package com.florangoutang.empirestrikesback.injection

import com.florangoutang.empirestrikesback.interfaceadapter.trip.common.StarNumberTransformer
import com.florangoutang.empirestrikesback.interfaceadapter.trip.detail.*
import com.florangoutang.empirestrikesback.interfaceadapter.util.scheduler.BaseSchedulerProvider
import com.florangoutang.empirestrikesback.ui.TripDetailFragment
import com.florangoutang.empirestrikesback.usecase.trip.TripRepository
import dagger.Module
import dagger.Provides

@Module
class TripDetailFragmentModule {

    @Provides
    fun provideFragment(): TripDetailFragment {
        return TripDetailFragment()
    }

    @Provides
    fun provideTripDetailPresenter(repository: TripRepository,
                                   listTransformer: TripDetailTransformer,
                                   schedulerProvider: BaseSchedulerProvider) : TripDetailContract.Presenter {
        return TripDetailPresenter(repository, listTransformer, schedulerProvider)
    }

    @Provides
    fun provideTripDetailTransformer(dateTransformer: DateTransformer,
                                     starNumberTransformer: StarNumberTransformer) : TripDetailTransformer {
        return TripDetailTransformer(dateTransformer, starNumberTransformer)
    }

    @Provides
    fun provideDateTransformer() = DateTransformer()
}
