package com.florangoutang.empirestrikesback.injection

import com.florangoutang.empirestrikesback.ui.TripDetailFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Subcomponent(modules = [TripDetailFragmentModule::class])
interface TripDetailFragmentComponent : AndroidInjector<TripDetailFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<TripDetailFragment>()
}