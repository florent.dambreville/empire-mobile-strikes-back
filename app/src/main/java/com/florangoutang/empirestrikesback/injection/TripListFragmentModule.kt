package com.florangoutang.empirestrikesback.injection

import com.florangoutang.empirestrikesback.interfaceadapter.trip.common.StarNumberTransformer
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.TripListContract
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.TripListPresenter
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.TripListTransformer
import com.florangoutang.empirestrikesback.interfaceadapter.util.scheduler.BaseSchedulerProvider
import com.florangoutang.empirestrikesback.interfaceadapter.util.scheduler.SchedulerProvider
import com.florangoutang.empirestrikesback.ui.TripListFragment
import com.florangoutang.empirestrikesback.usecase.trip.TripRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class TripListFragmentModule {

    @Provides
    fun provideFragment() : TripListFragment {
        return TripListFragment()
    }

    @Provides
    fun provideTripListPresenter(repository: TripRepository,
                                 listTransformer: TripListTransformer,
                                 schedulerProvider: BaseSchedulerProvider) : TripListContract.Presenter {
        return TripListPresenter(repository, listTransformer, schedulerProvider)
    }

    @Provides
    fun provideTripListTransformer(starNumberTransformer: StarNumberTransformer) =
            TripListTransformer(starNumberTransformer)

}
