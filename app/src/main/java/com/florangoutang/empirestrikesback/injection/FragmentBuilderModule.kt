package com.florangoutang.empirestrikesback.injection

import android.support.v4.app.Fragment
import com.florangoutang.empirestrikesback.ui.TripDetailFragment
import com.florangoutang.empirestrikesback.ui.TripListFragment
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap


@Module
abstract class FragmentBuilderModule {

    @Binds
    @IntoMap
    @FragmentKey(TripListFragment::class)
    internal abstract fun bindMainActivityToTripListFragment(builder: TripListFragmentComponent.Builder): AndroidInjector.Factory<out Fragment>

    @Binds
    @IntoMap
    @FragmentKey(TripDetailFragment::class)
    internal abstract fun bindMainActivityToTripDetailFragment(builder: TripDetailFragmentComponent.Builder): AndroidInjector.Factory<out Fragment>

}