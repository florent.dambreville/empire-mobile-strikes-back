package com.florangoutang.empirestrikesback.data

import android.arch.persistence.room.*
import com.florangoutang.empirestrikesback.entity.Trip
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface TripDao {

    @Query("SELECT * FROM trip WHERE id == :tripId ")
    fun getTripById(tripId: Int): Single<Trip>

    @Query("SELECT * FROM trip")
    fun getTripList(): Maybe<List<Trip>>

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insertAll(tripList: List<Trip>)

    @Update(onConflict = OnConflictStrategy.FAIL)
    fun update(tripList: List<Trip>)
}