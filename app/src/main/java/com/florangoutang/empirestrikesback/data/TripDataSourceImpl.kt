package com.florangoutang.empirestrikesback.data

import com.florangoutang.empirestrikesback.entity.Trip
import com.florangoutang.empirestrikesback.interfaceadapter.trip.common.TripRemoteTransformer
import com.florangoutang.empirestrikesback.usecase.trip.TripDataSource
import io.reactivex.Maybe
import io.reactivex.Single
import java.lang.Exception
import javax.inject.Inject

class TripDataSourceImpl @Inject constructor(val listTransformer : TripRemoteTransformer,
                                             val database: TripDatabase) : TripDataSource {

    private val tripRetrofitDataSource: TripRetrofitDataSource  by lazy { TripRetrofitDataSource.TripRetrofitApiBuilder().kaptenApi }

    override fun fetchTripList(): Single<List<Trip>> {
        return tripRetrofitDataSource.fetchTripList().map { listTransformer.tripRemoteToTripList(it) }
    }

    override fun getStoredTripList(): Maybe<List<Trip>> {
        return database.tripDao().getTripList()
    }

    override fun getTripDetail(tripId: Int): Single<Trip> {
        return database.tripDao().getTripById(tripId)
    }

    override fun storeTripList(tripList: List<Trip>) {
        database.tripDao().insertAll(tripList)
    }

    override fun updateTripList(tripList: List<Trip>) {
        database.tripDao().update(tripList)
    }
}