package com.florangoutang.empirestrikesback.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.florangoutang.empirestrikesback.entity.Trip

@Database(entities = [Trip::class], version = 1, exportSchema = false)
abstract class TripDatabase : RoomDatabase() {
    abstract fun tripDao(): TripDao
}