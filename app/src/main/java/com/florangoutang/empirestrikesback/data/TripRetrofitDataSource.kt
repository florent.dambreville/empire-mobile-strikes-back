package com.florangoutang.empirestrikesback.data

import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.model.TripRemoteModel
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface TripRetrofitDataSource {

    @GET("/trips")
    fun fetchTripList(): Single<List<TripRemoteModel>>

    class TripRetrofitApiBuilder {

        val kaptenApi: TripRetrofitDataSource

        init {
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor { chain ->
                val original = chain.request()
                val originalHttpUrl = original.url()
                val url = originalHttpUrl.newBuilder()
                        .build()
                val requestBuilder = original.newBuilder()
                        .header("Accept", "*/*;q=0.8")
                        .url(url)
                val request = requestBuilder.build()
                chain.proceed(request)
            }.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

            val retrofit = Retrofit.Builder()
                    .baseUrl("https://starwars.chauffeur-prive.com")
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()

            kaptenApi = retrofit.create(TripRetrofitDataSource::class.java)
        }


    }
}