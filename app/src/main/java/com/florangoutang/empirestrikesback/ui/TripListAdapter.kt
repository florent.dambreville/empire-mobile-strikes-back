package com.florangoutang.empirestrikesback.ui

import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.florangoutang.empirestrikesback.R
import com.florangoutang.empirestrikesback.ui.TripListAdapter.TripListCardViewHolder
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.model.TripListViewModel
import com.florangoutang.empirestrikesback.util.inflateFromParent
import com.florangoutang.empirestrikesback.util.loadUrl
import kotlinx.android.synthetic.main.trip_item.view.*
import kotlinx.android.synthetic.main.five_star_layout_small.view.*

class TripListAdapter(private val onTripClickedListener: TripListFragment.OnTripClickedListener)
    : RecyclerView.Adapter<TripListCardViewHolder>() {

    var tripList = mutableListOf<TripListViewModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TripListCardViewHolder(parent)

    override fun onBindViewHolder(holder: TripListCardViewHolder, position: Int) {
        holder.bind(tripList[position], onTripClickedListener)
    }

    override fun getItemCount() = tripList.size


    class TripListCardViewHolder(itemView: ViewGroup) : RecyclerView.ViewHolder(itemView.inflateFromParent(R.layout.trip_item)) {
        fun bind(tripListViewModel: TripListViewModel, onTripClickedListener: TripListFragment.OnTripClickedListener) = with(itemView) {
            pilotImageView.loadUrl(tripListViewModel.pilotImageUrl, R.drawable.pilot_placeholder)
            trip_item_container.setOnClickListener { onTripClickedListener.onTripClicked(tripListViewModel.id) }
            pilotNameTextView.text = tripListViewModel.pilotName
            departureTextView.text = tripListViewModel.tripDeparture
            arrivalTextView.text = tripListViewModel.tripArrival
            setStarFilledNumber(tripListViewModel.pilotRatingStarNumber)
        }

        private fun View.setStarFilledNumber(pilotStarNumberRating: Int) {
            when (pilotStarNumberRating) {
                0 -> {
                    starsContainer.visibility = View.GONE
                }
                1 -> {
                    firstStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_empty, null))
                    secondStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_empty, null))
                    thirdStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_empty, null))
                    fourthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_empty, null))
                    fifthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_empty, null))
                    starsContainer.visibility = View.VISIBLE
                }
                2 -> {
                    firstStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    secondStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    thirdStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_empty, null))
                    fourthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_empty, null))
                    fifthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_empty, null))
                    starsContainer.visibility = View.VISIBLE
                }
                3 -> {
                    firstStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    secondStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    thirdStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    fourthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_empty, null))
                    fifthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_empty, null))
                    starsContainer.visibility = View.VISIBLE
                }
                4 -> {
                    firstStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    secondStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    thirdStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    fourthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    fifthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_empty, null))
                    starsContainer.visibility = View.VISIBLE
                }
                5 -> {
                    firstStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    secondStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    thirdStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    fourthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    fifthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                    starsContainer.visibility = View.VISIBLE
                }
            }
        }
    }
}