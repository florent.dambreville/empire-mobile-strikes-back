package com.florangoutang.empirestrikesback.ui

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.florangoutang.empirestrikesback.R
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.TripListContract
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.model.TripListViewModel
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_trip_list.*
import javax.inject.Inject
import android.support.v4.content.ContextCompat



class TripListFragment : DaggerFragment(), TripListContract.View {

    @Inject lateinit var presenter: TripListContract.Presenter
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trip_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (activity is OnFragmentInteractionListener) {
            listener = activity as OnFragmentInteractionListener
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        configureRecyclerView()
        setupRefreshLayout()

        presenter.attachView(this)
        presenter.getTripList()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unsubscribe()
        presenter.detachView()
        listener = null
    }

    override fun showTripListError(message: String?) {
        setWidgetToDisplay(TripWidgetToDisplay.ERROR_MESSAGE)
        errorTextView.append("\n")
        errorTextView.append(message)
    }

    override fun showLoading(visible: Boolean) {
        swipeToRefreshLayout.isRefreshing = visible
    }

    override fun showTripList(list: MutableList<TripListViewModel>) {
        setWidgetToDisplay(TripWidgetToDisplay.LIST)
        (tripList.adapter as TripListAdapter).tripList = list
        (tripList.adapter as TripListAdapter).notifyDataSetChanged()
    }


    private fun setWidgetToDisplay(widgetToDisplay: TripWidgetToDisplay) {
        when (widgetToDisplay) {
            TripWidgetToDisplay.LIST -> {
                errorTextView.visibility = View.GONE
                tripList.visibility = View.VISIBLE
            }
            TripWidgetToDisplay.ERROR_MESSAGE -> {
                errorTextView.visibility = View.VISIBLE
                tripList.visibility = View.GONE
            }
        }
    }

    private fun configureRecyclerView() {
        if (tripList.adapter == null) {
            tripList.layoutManager = LinearLayoutManager(context)
            val dividerItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            dividerItemDecoration.setDrawable(ContextCompat.getDrawable(context!!, R.drawable.divider)!!)
            tripList.addItemDecoration(dividerItemDecoration)
            tripList.adapter = TripListAdapter(object : OnTripClickedListener {
                override fun onTripClicked(tripId: Int) {
                    listener?.onFragmentInteraction(tripId)
                }
            })
        }
    }

    private fun setupRefreshLayout() {
        swipeToRefreshLayout.setColorSchemeResources(R.color.colorPrimary)
        swipeToRefreshLayout.setOnRefreshListener {
            presenter.getTripList()
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(tripId: Int)
    }

    interface OnTripClickedListener {
        fun onTripClicked(tripId: Int)
    }
}

enum class TripWidgetToDisplay {
    LIST,
    ERROR_MESSAGE
}
