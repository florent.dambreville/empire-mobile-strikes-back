package com.florangoutang.empirestrikesback.ui

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.florangoutang.empirestrikesback.R
import com.florangoutang.empirestrikesback.interfaceadapter.trip.detail.TripDetailContract
import com.florangoutang.empirestrikesback.interfaceadapter.trip.detail.model.TripDetailViewModel
import com.florangoutang.empirestrikesback.util.loadUrl
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.five_star_layout_big.*
import kotlinx.android.synthetic.main.fragment_trip_detail.*
import javax.inject.Inject


class TripDetailFragment : DaggerFragment(), TripDetailContract.View {

    @Inject lateinit var presenter : TripDetailContract.Presenter
    private var tripId : Int? = null

    companion object {
        private const val ARG_TRIP_ID = "param1"

        @JvmStatic
        fun newInstance(tripId: Int) =
                TripDetailFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_TRIP_ID, tripId)
                    }
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            tripId = it.getInt(ARG_TRIP_ID)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trip_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (activity as MainActivity).setSupportActionBar(toolbar)
        toolbar.title = ""
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener { activity?.onBackPressed() }

        presenter.attachView(this)
        tripId?.let {  presenter.getTripDetail(it) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unsubscribe()
        presenter.detachView()
    }

    override fun tripDetailError(message: String?) {
        errorDetailTextView.visibility = View.VISIBLE
        errorDetailTextView.append("\n")
        errorDetailTextView.append(message)
    }

    override fun showLoading() {
        errorDetailTextView.visibility = View.GONE
        detailLoader.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        errorDetailTextView.visibility = View.GONE
        detailLoader.visibility = View.GONE
    }

    override fun showTripDetail(tripDetailViewModel: TripDetailViewModel) {
        errorDetailTextView.visibility = View.GONE
        with(tripDetailViewModel) {
            pilotImageView.loadUrl(tripDetailViewModel.pilotImageUrl, R.drawable.pilot_placeholder)
            pilotNameTextView.text = pilotName
            departureTextView.text = tripDeparture
            departureTimeTextView.text = departureDate
            arrivalTextView.text = tripArrival
            arrivalTimeTextView.text = arrivalDate
            distanceTextView.text = distance
            durationTextView.text = duration
            setStarFilledNumber(pilotRatingStarNumber)
            setDepartureImageView(tripDeparture)
            setArrivalImageView(tripArrival)
        }
    }

    private fun setDepartureImageView(tripDeparture: String) {
        val drawable = getDrawableFromPlanetName(tripDeparture)
        if (context != null) departurePlanetImageView.setImageDrawable(ContextCompat.getDrawable(context!!, drawable))
    }

    private fun setArrivalImageView(tripArrival: String) {
        val drawable = getDrawableFromPlanetName(tripArrival)
        if (context != null) arrivalPlanetImageView.setImageDrawable(ContextCompat.getDrawable(context!!, drawable))
    }

    private fun getDrawableFromPlanetName(tripDeparture: String): Int {
        return when (tripDeparture.toLowerCase()) {
            "tatooine" -> R.drawable.tatooine
            "coruscant" -> R.drawable.coruscant
            "hoth" -> R.drawable.hoth
            "yavin 4" -> R.drawable.yavin_4
            "naboo" -> R.drawable.naboo
            else -> R.drawable.earth
        }
    }

    private fun setStarFilledNumber(pilotRating: Int) {
        when(pilotRating) {
            0 -> {
                starsContainer.visibility = View.GONE
                noRating.visibility = View.VISIBLE
            }
            1 -> {
                firstStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                starsContainer.visibility = View.VISIBLE
                noRating.visibility = View.GONE
            }
            2 -> {
                firstStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                secondStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                starsContainer.visibility = View.VISIBLE
                noRating.visibility = View.GONE
            }
            3 -> {
                firstStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                secondStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                thirdStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                starsContainer.visibility = View.VISIBLE
                noRating.visibility = View.GONE
            }
            4 -> {
                firstStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                secondStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                thirdStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                fourthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                starsContainer.visibility = View.VISIBLE
                noRating.visibility = View.GONE
            }
            5 -> {
                firstStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                secondStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                thirdStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                fourthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                fifthStarImageView.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.star_filled, null))
                starsContainer.visibility = View.VISIBLE
                noRating.visibility = View.GONE
            }
        }
    }

}
