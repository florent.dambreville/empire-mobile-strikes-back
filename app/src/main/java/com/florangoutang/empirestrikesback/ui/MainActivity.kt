package com.florangoutang.empirestrikesback.ui

import android.os.Bundle
import com.florangoutang.empirestrikesback.R
import dagger.android.support.DaggerAppCompatActivity


class MainActivity : DaggerAppCompatActivity(), TripListFragment.OnFragmentInteractionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, TripListFragment())
                .commit()
    }

    override fun onFragmentInteraction(tripId: Int) {
        val newFragment = TripDetailFragment.newInstance(tripId)
        supportFragmentManager.beginTransaction()
                .add(R.id.fragmentContainer, newFragment)
                .addToBackStack(newFragment.tag)
                .commit()
    }

}
