package com.florangoutang.empirestrikesback.interfaceadapter.trip.common

import com.florangoutang.empirestrikesback.entity.Distance
import com.florangoutang.empirestrikesback.entity.Pilot
import com.florangoutang.empirestrikesback.entity.Planet
import com.florangoutang.empirestrikesback.entity.Trip
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.model.TripRemoteModel
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class TripRemoteTransformerTest {

    private val transformer = TripRemoteTransformer()

    @Test
    fun `tripRemoteToTripList when expected remote model is valid should return tripList with same data as remote model`() {
        // Given
        val remotePilot = mock<TripRemoteModel.RemotePilot> {
            on { name } doReturn "pilot name"
            on { avatarUrl } doReturn "imageUrl url"
            on { rating } doReturn 5f
        }
        val remoteDistance = mock<TripRemoteModel.RemoteDistance> {
            on { value } doReturn 42
            on { unit } doReturn "unit"
        }
        val remoteDeparture = mock<TripRemoteModel.RemoteLocationAtSpecificMoment> {
            on { name } doReturn "departure name"
            on { locationImageUrl } doReturn "departure image url"
            on { date } doReturn "departure date"
        }
        val remoteArrival = mock<TripRemoteModel.RemoteLocationAtSpecificMoment> {
            on { name } doReturn "arrival name"
            on { locationImageUrl } doReturn "arrival image url"
            on { date } doReturn "arrival date"
        }
        val remoteTrip = mock<TripRemoteModel> {
            on { id } doReturn 123
            on { pilot } doReturn remotePilot
            on { distance } doReturn remoteDistance
            on { duration } doReturn 56
            on { departure } doReturn remoteDeparture
            on { arrival } doReturn remoteArrival
        }

        // When
        val tripList = transformer.tripRemoteToTripList(mutableListOf(remoteTrip))

        // Then
        assertThat(tripList.size).isEqualTo(1)
        with(tripList.first()) {
            assertThat(id).isEqualTo(123)
            assertThat(pilot.name).isEqualTo("pilot name")
            assertThat(pilot.imageUrl).isEqualTo("imageUrl url")
            assertThat(pilot.rating).isEqualTo(5f)
            assertThat(distance.value).isEqualTo(42)
            assertThat(distance.unit).isEqualTo("unit")
            assertThat(duration).isEqualTo(56)
            assertThat(departure.name).isEqualTo("departure name")
            assertThat(departureDate).isEqualTo("departure date")
            assertThat(arrival.name).isEqualTo("arrival name")
            assertThat(arrivalDate).isEqualTo("arrival date")

        }
    }

    @Test
    fun `tripRemoteToTripList when invalid remote model return tripList with default`() {
        // Given
        val remoteTrip = mock<TripRemoteModel>()

        // When
        val tripList = transformer.tripRemoteToTripList(mutableListOf(remoteTrip))

        // Then
        assertThat(tripList.size).isEqualTo(1)
        assertThat(tripList.first()).isEqualTo(
                Trip(id = 0,
                        pilot = Pilot("", "", 0f),
                        distance = Distance(0, ""),
                        duration = 0,
                        departure = Planet(""),
                        departureDate = "",
                        arrival = Planet(""),
                        arrivalDate = ""
                )
        )
    }

    @Test
    fun `tripRemoteToTripList when two valid remote model with same id return tripList with different id`() {
        // Given
        val remoteTrip = mock<TripRemoteModel> {
            on { id } doReturn 5
        }
        val remoteTrip2 = mock<TripRemoteModel> {
            on { id } doReturn 6
        }

        // When
        val tripList = transformer.tripRemoteToTripList(mutableListOf(remoteTrip, remoteTrip, remoteTrip2))

        // Then
        assertThat(tripList.size).isEqualTo(3)
        assertThat(tripList.first()).isEqualTo(
                Trip(id = 5,
                        pilot = Pilot("", "", 0f),
                        distance = Distance(0, ""),
                        duration = 0,
                        departure = Planet(""),
                        departureDate = "",
                        arrival = Planet(""),
                        arrivalDate = ""
                )
        )
        assertThat(tripList[1]).isEqualTo(
                Trip(id = 6,
                        pilot = Pilot("", "", 0f),
                        distance = Distance(0, ""),
                        duration = 0,
                        departure = Planet(""),
                        departureDate = "",
                        arrival = Planet(""),
                        arrivalDate = ""
                )
        )
        assertThat(tripList[2]).isEqualTo(
                Trip(id = 7,
                        pilot = Pilot("", "", 0f),
                        distance = Distance(0, ""),
                        duration = 0,
                        departure = Planet(""),
                        departureDate = "",
                        arrival = Planet(""),
                        arrivalDate = ""
                )
        )
    }
}