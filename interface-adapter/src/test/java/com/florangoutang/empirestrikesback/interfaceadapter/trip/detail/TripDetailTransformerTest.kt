package com.florangoutang.empirestrikesback.interfaceadapter.trip.detail

import com.florangoutang.empirestrikesback.entity.Distance
import com.florangoutang.empirestrikesback.entity.Pilot
import com.florangoutang.empirestrikesback.entity.Planet
import com.florangoutang.empirestrikesback.entity.Trip
import com.florangoutang.empirestrikesback.interfaceadapter.trip.common.StarNumberTransformer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TripDetailTransformerTest {

    @InjectMocks lateinit var transformer : TripDetailTransformer
    @Mock lateinit var dateTransformer: DateTransformer
    @Mock lateinit var starNumberTransformer: StarNumberTransformer

    @Test
    fun `tripToTripDetailViewModel with a Trip should return expecting TripDetailViewModel`() {
        // Given
        given(dateTransformer.rawDateToHour("departure date"))
                .willReturn("departure hour")
        given(dateTransformer.rawDateToHour("arrival date"))
                .willReturn("arrival hour")
        given(dateTransformer.millisSecondsToHourMinuteSeconds(1000))
                .willReturn("00:00:01")
        given(starNumberTransformer.ratingToStarNumber(4.5f)).willReturn(55)
        val distance = mock<Distance> {
            on { value } doReturn 42
            on { unit } doReturn "unit"
        }
        val departure = mock<Planet> {
            on { name } doReturn "departure name"
        }
        val arrival = mock<Planet> {
            on { name } doReturn "arrival name"
        }
        val pilot = mock<Pilot> {
            on { imageUrl } doReturn "pilot image url"
            on { name } doReturn "pilot name"
            on { rating } doReturn 4.5f
        }
        val trip = mock<Trip> {
            on { id } doReturn 42
            on { this.pilot } doReturn pilot
            on { this.departure } doReturn departure
            on { this.departureDate } doReturn "departure date"
            on { this.arrival } doReturn arrival
            on { this.arrivalDate } doReturn "arrival date"
            on { this.distance } doReturn distance
            on { this.duration } doReturn 1000
        }

        // When
        val tripViewModel = transformer.tripToTripDetailViewModel(trip)

        // Then
        with(tripViewModel) {
            assertThat(id).isEqualTo(42)
            assertThat(pilotImageUrl).isEqualTo("pilot image url")
            assertThat(pilotName).isEqualTo("pilot name")
            assertThat(tripDeparture).isEqualTo("departure name")
            assertThat(departureDate).isEqualTo("departure hour")
            assertThat(tripArrival).isEqualTo("arrival name")
            assertThat(arrivalDate).isEqualTo("arrival hour")
            assertThat(this.distance).isEqualTo("42 unit")
            assertThat(duration).isEqualTo("00:00:01")
            assertThat(pilotRatingStarNumber).isEqualTo(55)
        }
    }
}