package com.florangoutang.empirestrikesback.interfaceadapter.trip.detail

import com.florangoutang.empirestrikesback.interfaceadapter.util.TestSchedulerProvider
import com.florangoutang.empirestrikesback.interfaceadapter.util.scheduler.BaseSchedulerProvider
import com.florangoutang.empirestrikesback.usecase.trip.TripRepository
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.then
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TripDetailPresenterTest {

    @InjectMocks lateinit var presenter: TripDetailPresenter
    @Mock lateinit var repository: TripRepository
    @Mock lateinit var schedulerProvider: BaseSchedulerProvider
    @Mock lateinit var view: TripDetailContract.View
    @Mock lateinit var detailTransformer: TripDetailTransformer
    private val testSchedulers = TestSchedulerProvider()

    @Before
    fun setUp() {
        presenter.attachView(view)
        given(schedulerProvider.computation()).willReturn(testSchedulers.computation())
        given(schedulerProvider.ui()).willReturn(testSchedulers.ui())
    }

    @Test
    fun `getTripDetail should show loading and then call repository`() {
        // Given
        given(repository.getTripDetail(1)).willReturn(Single.just(mock()))

        // When
        presenter.getTripDetail(1)
        testSchedulers.computation().triggerActions()

        // Then
        inOrder(view, repository) {
            then(view).should().showLoading()
            then(repository).should().getTripDetail(1)
        }
    }
}