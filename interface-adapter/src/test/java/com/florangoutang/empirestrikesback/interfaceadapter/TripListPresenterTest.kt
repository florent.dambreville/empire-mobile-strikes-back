package com.florangoutang.empirestrikesback.interfaceadapter

import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.TripListContract
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.TripListPresenter
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.TripListTransformer
import com.florangoutang.empirestrikesback.interfaceadapter.util.TestSchedulerProvider
import com.florangoutang.empirestrikesback.interfaceadapter.util.scheduler.BaseSchedulerProvider
import com.florangoutang.empirestrikesback.usecase.trip.TripRepository
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.then
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TripListPresenterTest {

    @InjectMocks lateinit var presenter: TripListPresenter
    @Mock lateinit var repository: TripRepository
    @Mock lateinit var schedulerProvider: BaseSchedulerProvider
    @Mock lateinit var view: TripListContract.View
    @Mock lateinit var listTransformer: TripListTransformer
    private val testSchedulers = TestSchedulerProvider()

    @Before
    fun setUp() {
        presenter.attachView(view)
        given(schedulerProvider.computation()).willReturn(testSchedulers.computation())
        given(schedulerProvider.ui()).willReturn(testSchedulers.ui())
    }

    @Test
    fun `getTripList should show loading and then call repository`() {
        // Given
        given(repository.getTripList()).willReturn(Single.just(listOf()))

        // When
        presenter.getTripList()
        testSchedulers.computation().triggerActions()

        // Then
        inOrder(view, repository) {
            then(view).should().showLoading(true)
            then(repository).should().getTripList()
        }
    }
}