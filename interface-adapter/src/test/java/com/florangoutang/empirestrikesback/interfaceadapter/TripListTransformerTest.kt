package com.florangoutang.empirestrikesback.interfaceadapter

import com.florangoutang.empirestrikesback.entity.Pilot
import com.florangoutang.empirestrikesback.entity.Planet
import com.florangoutang.empirestrikesback.entity.Trip
import com.florangoutang.empirestrikesback.interfaceadapter.trip.common.StarNumberTransformer
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.TripListTransformer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TripListTransformerTest {

    @InjectMocks lateinit var transformer : TripListTransformer
    @Mock lateinit var starNumberTransformer: StarNumberTransformer

    @Test
    fun `tripToTripViewModel with Trip should return TripViewModel`() {
        // Given
        given(starNumberTransformer.ratingToStarNumber(1f)).willReturn(88)
        val pilot = mock<Pilot> {
            on { imageUrl } doReturn "image url"
            on { name } doReturn "pilot name"
            on { rating } doReturn 1f
        }
        val departure = mock<Planet> {
            on { name } doReturn "departure name"
        }
        val arrival = mock<Planet> {
            on { name } doReturn "arrival name"
        }
        val trip = mock<Trip> {
            on { id } doReturn 42
            on { this.pilot } doReturn pilot
            on { this.departure } doReturn departure
            on { this.arrival } doReturn arrival
        }

        // When
        val tripListViewModel = transformer.tripToTripListViewModel(trip)

        // Then
        with(tripListViewModel) {
            assertThat(id).isEqualTo(42)
            assertThat(pilotImageUrl).isEqualTo("image url")
            assertThat(pilotName).isEqualTo("pilot name")
            assertThat(tripDeparture).isEqualTo("departure name")
            assertThat(tripArrival).isEqualTo("arrival name")
            assertThat(pilotRatingStarNumber).isEqualTo(88)
        }
    }
}