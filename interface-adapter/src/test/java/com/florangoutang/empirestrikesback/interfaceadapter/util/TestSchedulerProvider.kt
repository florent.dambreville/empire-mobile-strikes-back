package com.florangoutang.empirestrikesback.interfaceadapter.util

import com.florangoutang.empirestrikesback.interfaceadapter.util.scheduler.BaseSchedulerProvider
import io.reactivex.schedulers.TestScheduler

class TestSchedulerProvider : BaseSchedulerProvider {

    override fun computation()= TestScheduler()

    override fun io() = TestScheduler()

    override fun ui() = TestScheduler()

}