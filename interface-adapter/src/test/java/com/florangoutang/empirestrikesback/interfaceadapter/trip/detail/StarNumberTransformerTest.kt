package com.florangoutang.empirestrikesback.interfaceadapter.trip.detail

import com.florangoutang.empirestrikesback.interfaceadapter.trip.common.StarNumberTransformer
import com.tngtech.java.junit.dataprovider.DataProvider
import com.tngtech.java.junit.dataprovider.DataProviderRunner
import com.tngtech.java.junit.dataprovider.UseDataProvider
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(DataProviderRunner::class)
class StarNumberTransformerTest {

    private val transformer = StarNumberTransformer()

    companion object {
        @DataProvider
        @JvmStatic
        @Suppress("unused")
        fun mapPilotRating(): Array<Array<out Any?>> = arrayOf(
                arrayOf(0f, 0),
                arrayOf(0.5f, 1),
                arrayOf(1.4f, 1),
                arrayOf(1.5f, 2),
                arrayOf(2.2f, 2),
                arrayOf(2.8f, 3),
                arrayOf(3.3f, 3),
                arrayOf(3.6f, 4),
                arrayOf(4f, 4),
                arrayOf(4.2f, 4),
                arrayOf(4.9f, 5),
                arrayOf(5f, 5),
                arrayOf(14f, 0),
                arrayOf(42f, 0),
                arrayOf(1000000f, 0)
        )
    }

    @Test
    @UseDataProvider("mapPilotRating")
    fun `ratingToStarNumber when rating at 0 should return 0`(pilotRating: Float, expectedStarNumber: Int) {
        // When
        val starNumber = transformer.ratingToStarNumber(pilotRating)

        // Then
        assertThat(starNumber).isEqualTo(expectedStarNumber)
    }
}