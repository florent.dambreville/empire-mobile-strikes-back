package com.florangoutang.empirestrikesback.interfaceadapter.trip.detail

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class DateTransformerTest {

    private val transformer = DateTransformer()

    @Test
    fun `rawDateToHour when valid date entry should return corresponding hour`() {
        // Given
        val dateEntry = "2017-12-09T19:35:51Z"

        // When
        val hour = transformer.rawDateToHour(dateEntry)

        // Then
        assertThat(hour).isEqualTo("07:35 PM")
    }

    @Test
    fun `rawDateToHour when invalid date entry should return empty string`() {
        // Given
        val dateEntry = "2017-12-09"

        // When
        val hour = transformer.rawDateToHour(dateEntry)

        // Then
        assertThat(hour).isEqualTo("")
    }

    @Test
    fun `secondsToHourMinuteSeconds should return hour minute and seconds`() {
        // Given
        val duration = 19427000

        // When
        val hoursMinutesSeconds = transformer.millisSecondsToHourMinuteSeconds(duration.toLong())

        //Then
        assertThat(hoursMinutesSeconds).isEqualTo("5:23:47")
    }

    @Test
    fun `hoursBetweenTwoDates when two invalid date should return hour between two dates`() {
        // Given
        val duration = 0

        // When
        val hoursBetweenTwoDates = transformer.millisSecondsToHourMinuteSeconds(duration.toLong())

        //Then
        assertThat(hoursBetweenTwoDates).isEqualTo("0:0:0")
    }
}