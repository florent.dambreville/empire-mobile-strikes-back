package com.florangoutang.empirestrikesback.interfaceadapter.trip.list

import com.florangoutang.empirestrikesback.interfaceadapter.base.BasePresenter
import com.florangoutang.empirestrikesback.interfaceadapter.base.BaseView
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.model.TripListViewModel

interface TripListContract {

    interface View : BaseView {
        fun showTripListError(message: String?)
        fun showLoading(visible: Boolean)
        fun showTripList(list: MutableList<TripListViewModel>)
    }

    interface Presenter : BasePresenter<View> {
        fun getTripList()
    }
}