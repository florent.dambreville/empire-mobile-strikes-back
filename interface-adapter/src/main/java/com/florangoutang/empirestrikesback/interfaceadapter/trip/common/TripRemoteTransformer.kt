package com.florangoutang.empirestrikesback.interfaceadapter.trip.common

import com.florangoutang.empirestrikesback.entity.Distance
import com.florangoutang.empirestrikesback.entity.Pilot
import com.florangoutang.empirestrikesback.entity.Planet
import com.florangoutang.empirestrikesback.entity.Trip
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.model.TripRemoteModel
import com.florangoutang.empirestrikesback.interfaceadapter.util.containsTwo
import com.florangoutang.empirestrikesback.interfaceadapter.util.emptyOrValue
import com.florangoutang.empirestrikesback.interfaceadapter.util.zeroOrValue

class TripRemoteTransformer {

    fun tripRemoteToTripList(tripRemoteModelList: List<TripRemoteModel>): List<Trip> {
        val existingTripIdList = mutableListOf<Int>()
        val tripList = mutableListOf<Trip>()
        tripRemoteModelList.forEach { remoteTrip ->
            existingTripIdList.add(remoteTrip.id.zeroOrValue())
            tripList.add(Trip(
                    id = modifyDuplicatedId(remoteTrip.id.zeroOrValue(), existingTripIdList),
                    pilot = Pilot(remoteTrip.pilot?.name.emptyOrValue(),
                            remoteTrip.pilot?.avatarUrl.emptyOrValue(),
                            remoteTrip.pilot?.rating.zeroOrValue()),
                    distance = Distance(remoteTrip.distance?.value.zeroOrValue(),
                            remoteTrip.distance?.unit.emptyOrValue()),
                    duration = remoteTrip.duration.zeroOrValue(),
                    departure = Planet(remoteTrip.departure?.name.emptyOrValue()),
                    departureDate = remoteTrip.departure?.date.emptyOrValue(),
                    arrival = Planet(remoteTrip.arrival?.name.emptyOrValue()),
                    arrivalDate = remoteTrip.arrival?.date.emptyOrValue()
            ))
        }
        return tripList
    }

    private fun modifyDuplicatedId(tripId: Int, existingIdList: MutableList<Int>): Int {
        return if (existingIdList.containsTwo(tripId)) {
            existingIdList.remove(tripId)
            existingIdList.add(tripId + 1)
            tripId + 1
        } else {
            tripId
        }
    }

}