package com.florangoutang.empirestrikesback.interfaceadapter.trip.detail

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class DateTransformer {

    fun rawDateToHour(dateString: String) : String {
        return try {
            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.FRENCH)
            val date = inputFormat.parse(dateString)
            val outputFormat = SimpleDateFormat("hh:mm aa", Locale.US)
            outputFormat.format(date)
        } catch (e : ParseException) {
            ""
        }
    }

    fun millisSecondsToHourMinuteSeconds(durationInMillisSeconds: Long) : String {
        val day = TimeUnit.MILLISECONDS.toDays(durationInMillisSeconds)
        val hour = TimeUnit.MILLISECONDS.toHours(durationInMillisSeconds) - TimeUnit.DAYS.toHours(day)
        val minute = TimeUnit.MILLISECONDS.toMinutes(durationInMillisSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(durationInMillisSeconds))
        val second = TimeUnit.MILLISECONDS.toSeconds(durationInMillisSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(durationInMillisSeconds))
        return "$hour:$minute:$second"
    }
}
