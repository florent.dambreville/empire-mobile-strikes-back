package com.florangoutang.empirestrikesback.interfaceadapter.trip.common

import com.florangoutang.empirestrikesback.interfaceadapter.util.contains
import com.florangoutang.empirestrikesback.interfaceadapter.util.until

class StarNumberTransformer {

    fun ratingToStarNumber(pilotRating: Float) : Int {
        return when(pilotRating) {
            0f -> 0
            in 0.1f until 1.4f -> 1
            in 1.5f until 2.4f -> 2
            in 2.5f until 3.4f -> 3
            in 3.5f until 4.4f -> 4
            in 4.5f until 5f -> 5
            else -> 0
        }
    }

}
