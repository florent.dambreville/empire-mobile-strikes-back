package com.florangoutang.empirestrikesback.interfaceadapter.trip.detail

import com.florangoutang.empirestrikesback.interfaceadapter.trip.detail.model.TripDetailViewModel
import com.florangoutang.empirestrikesback.interfaceadapter.util.scheduler.BaseSchedulerProvider
import com.florangoutang.empirestrikesback.usecase.trip.TripRepository
import io.reactivex.disposables.CompositeDisposable

class TripDetailPresenter(val repository: TripRepository,
                          val transformer: TripDetailTransformer,
                          val schedulersProvider : BaseSchedulerProvider) : TripDetailContract.Presenter {

    override var view: TripDetailContract.View? = null

    private val subscriptions: CompositeDisposable = CompositeDisposable()

    override fun attachView(view: TripDetailContract.View?) {
        this.view = view
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun getTripDetail(tripId: Int) {
        view?.showLoading()
        subscriptions.add(repository.getTripDetail(tripId)
                .subscribeOn(schedulersProvider.computation())
                .observeOn(schedulersProvider.ui())
                .doFinally { view?.hideLoading() }
                .map { transformer.tripToTripDetailViewModel(it) }
                .subscribe(
                        { trip: TripDetailViewModel ->
                            view?.showTripDetail(trip)
                        },
                        { error: Throwable ->
                            error.printStackTrace()
                            view?.tripDetailError(error.message)
                        }
                )
        )
    }
}