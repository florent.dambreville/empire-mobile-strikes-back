package com.florangoutang.empirestrikesback.interfaceadapter.trip.detail

import com.florangoutang.empirestrikesback.entity.Trip
import com.florangoutang.empirestrikesback.interfaceadapter.trip.common.StarNumberTransformer
import com.florangoutang.empirestrikesback.interfaceadapter.trip.detail.model.TripDetailViewModel
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

class TripDetailTransformer @Inject constructor(val dateTransformer: DateTransformer,
                                                val starNumberTransformer: StarNumberTransformer) {

    fun tripToTripDetailViewModel(trip: Trip): TripDetailViewModel {
        with(trip) {
            val distanceValueFormattedWithComa = NumberFormat.getNumberInstance(Locale.US).format(distance.value)
            val distance = "$distanceValueFormattedWithComa ${distance.unit}"
            return TripDetailViewModel(
                    id = id,
                    pilotImageUrl = pilot.imageUrl,
                    pilotName = pilot.name,
                    tripDeparture = departure.name,
                    departureDate = dateTransformer.rawDateToHour(departureDate),
                    tripArrival = arrival.name,
                    arrivalDate = dateTransformer.rawDateToHour(arrivalDate),
                    distance = distance,
                    duration = dateTransformer.millisSecondsToHourMinuteSeconds(duration),
                    pilotRatingStarNumber = starNumberTransformer.ratingToStarNumber(pilot.rating)
            )
        }
    }
}