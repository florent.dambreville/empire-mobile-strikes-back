package com.florangoutang.empirestrikesback.interfaceadapter.trip.list.model

data class TripListViewModel(val id: Int,
                             val pilotImageUrl: String,
                             val pilotName: String,
                             val tripDeparture: String,
                             val tripArrival: String,
                             val pilotRatingStarNumber: Int)