package com.florangoutang.empirestrikesback.interfaceadapter.trip.list.model

import com.google.gson.annotations.SerializedName

data class TripRemoteModel(val id: Int?,
                           val pilot: RemotePilot?,
                           val distance: RemoteDistance?,
                           val duration: Long?,
                           @SerializedName("pick_up") val departure: RemoteLocationAtSpecificMoment?,
                           @SerializedName("drop_off") val arrival: RemoteLocationAtSpecificMoment?) {


        data class RemotePilot(val name: String?,
                               @SerializedName("avatar") val avatarUrl: String?,
                               val rating: Float?)

        data class RemoteDistance(val value: Long?,
                                  val unit: String?)

        data class RemoteLocationAtSpecificMoment(val name: String?,
                                                  val locationImageUrl: String?,
                                                  val date: String?)
}

