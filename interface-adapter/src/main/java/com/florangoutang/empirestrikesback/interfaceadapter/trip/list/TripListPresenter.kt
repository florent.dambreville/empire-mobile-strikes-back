package com.florangoutang.empirestrikesback.interfaceadapter.trip.list

import com.florangoutang.empirestrikesback.entity.Trip
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.model.TripListViewModel
import com.florangoutang.empirestrikesback.interfaceadapter.util.scheduler.BaseSchedulerProvider
import com.florangoutang.empirestrikesback.usecase.trip.TripRepository
import io.reactivex.disposables.CompositeDisposable

class TripListPresenter(val repository: TripRepository,
                        val listTransformer: TripListTransformer,
                        val schedulersProvider : BaseSchedulerProvider) : TripListContract.Presenter {

    override var view: TripListContract.View? = null

    private val subscriptions: CompositeDisposable = CompositeDisposable()

    override fun attachView(view: TripListContract.View?) {
        this.view = view
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun getTripList() {
        view?.showLoading(true)
        subscriptions.add(repository.getTripList()
                .subscribeOn(schedulersProvider.computation())
                .observeOn(schedulersProvider.ui())
                .doFinally { view?.showLoading(false) }
                .map { tripList: List<Trip> ->
                    tripList.map { listTransformer.tripToTripListViewModel(it) }
                }
                .subscribe(
                        { tripListViewModel: List<TripListViewModel> ->
                            view?.showTripList(tripListViewModel.toMutableList())
                        },
                        { error: Throwable ->
                            error.printStackTrace()
                            view?.showTripListError(error.message)

                        }
                )
        )
    }
}