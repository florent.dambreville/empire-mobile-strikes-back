package com.florangoutang.empirestrikesback.interfaceadapter.util

data class OpenFloatRange(val from: Float, val to: Float)
operator fun OpenFloatRange.contains(f: Float) = f in from..to
infix fun Float.until(to: Float) = OpenFloatRange(this, to)
