package com.florangoutang.empirestrikesback.interfaceadapter.trip.list

import com.florangoutang.empirestrikesback.entity.Trip
import com.florangoutang.empirestrikesback.interfaceadapter.trip.common.StarNumberTransformer
import com.florangoutang.empirestrikesback.interfaceadapter.trip.list.model.TripListViewModel

class TripListTransformer(val starNumberTransformer: StarNumberTransformer) {

    fun tripToTripListViewModel(trip: Trip): TripListViewModel {
        with(trip) {
            return TripListViewModel(id = id,
                    pilotImageUrl = pilot.imageUrl,
                    pilotName = pilot.name,
                    tripDeparture = departure.name,
                    tripArrival = arrival.name,
                    pilotRatingStarNumber = starNumberTransformer.ratingToStarNumber(pilot.rating))
        }
    }
}