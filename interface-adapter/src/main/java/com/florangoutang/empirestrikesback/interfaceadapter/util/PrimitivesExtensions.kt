package com.florangoutang.empirestrikesback.interfaceadapter.util

fun Int?.zeroOrValue() : Int {
    return when(this) {
        null -> 0
        else -> this
    }
}

fun Long?.zeroOrValue() : Long {
    return when(this) {
        null -> 0
        else -> this
    }
}

fun Float?.zeroOrValue() : Float {
    return when(this) {
        null -> 0f
        else -> this
    }
}

fun List<Int>.containsTwo(int: Int) : Boolean {
    val duplicatedList = this.toMutableList()
    return if (duplicatedList.contains(int)) {
        duplicatedList.remove(int)
        duplicatedList.contains(int)
    } else {
        false
    }
}

fun String?.emptyOrValue() : String {
    return when(this) {
        null -> ""
        else -> this
    }
}