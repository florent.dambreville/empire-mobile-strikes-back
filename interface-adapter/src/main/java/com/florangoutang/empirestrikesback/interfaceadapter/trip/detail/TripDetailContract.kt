package com.florangoutang.empirestrikesback.interfaceadapter.trip.detail

import com.florangoutang.empirestrikesback.interfaceadapter.trip.detail.model.TripDetailViewModel
import com.florangoutang.empirestrikesback.interfaceadapter.base.BasePresenter
import com.florangoutang.empirestrikesback.interfaceadapter.base.BaseView

interface TripDetailContract {

    interface View : BaseView {
        fun tripDetailError(message: String?)
        fun showLoading()
        fun hideLoading()
        fun showTripDetail(tripDetailViewModel: TripDetailViewModel)
    }

    interface Presenter : BasePresenter<View> {
        fun getTripDetail(tripId: Int)
    }
}