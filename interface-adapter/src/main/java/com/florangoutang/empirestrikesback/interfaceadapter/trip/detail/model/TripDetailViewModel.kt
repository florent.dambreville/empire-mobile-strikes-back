package com.florangoutang.empirestrikesback.interfaceadapter.trip.detail.model

data class TripDetailViewModel(val id: Int,
                               val pilotImageUrl: String,
                               val pilotName: String,
                               val tripDeparture: String,
                               val departureDate: String,
                               val tripArrival: String,
                               val arrivalDate: String,
                               val distance: String,
                               val duration: String,
                               val pilotRatingStarNumber: Int)