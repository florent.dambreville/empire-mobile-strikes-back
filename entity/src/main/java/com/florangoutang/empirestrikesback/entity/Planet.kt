package com.florangoutang.empirestrikesback.entity

import android.arch.persistence.room.ColumnInfo

data class Planet(@ColumnInfo(name = "planet_name") val name: String)