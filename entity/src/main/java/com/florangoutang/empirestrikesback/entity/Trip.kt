package com.florangoutang.empirestrikesback.entity

import android.arch.persistence.room.*

@Entity(primaryKeys = ["id", "pilot_name"])
data class Trip(val id: Int,
                @Embedded val pilot: Pilot,
                @Embedded val distance: Distance,
                @ColumnInfo(name = "duration") val duration: Long,
                @Embedded(prefix = "departure_") val departure: Planet,
                @ColumnInfo(name = "departure_date") val departureDate: String,
                @Embedded(prefix = "arrival_") val arrival: Planet,
                @ColumnInfo(name = "arrival_date") val arrivalDate: String)
