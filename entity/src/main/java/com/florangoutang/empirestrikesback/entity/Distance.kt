package com.florangoutang.empirestrikesback.entity

data class Distance(val value: Long,
                    val unit: String)