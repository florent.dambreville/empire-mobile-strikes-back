package com.florangoutang.empirestrikesback.entity

import android.arch.persistence.room.ColumnInfo

data class Pilot(@ColumnInfo(name = "pilot_name") val name: String,
                 @ColumnInfo(name = "pilot_image_url") val imageUrl: String,
                 val rating: Float)