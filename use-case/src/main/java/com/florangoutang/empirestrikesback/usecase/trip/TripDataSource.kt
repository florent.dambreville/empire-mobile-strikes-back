package com.florangoutang.empirestrikesback.usecase.trip

import com.florangoutang.empirestrikesback.entity.Trip
import io.reactivex.Maybe
import io.reactivex.Single

interface TripDataSource {
    fun fetchTripList(): Single<List<Trip>>
    fun getStoredTripList(): Maybe<List<Trip>>
    fun getTripDetail(tripId: Int): Single<Trip>
    fun storeTripList(tripList: List<Trip>)
    fun updateTripList(tripList: List<Trip>)
}