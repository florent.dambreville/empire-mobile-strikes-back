package com.florangoutang.empirestrikesback.usecase.trip

import android.database.sqlite.SQLiteConstraintException
import com.florangoutang.empirestrikesback.entity.Trip
import io.reactivex.Single

interface TripRepository {
    fun getTripList(): Single<List<Trip>>
    fun getTripDetail(tripId: Int): Single<Trip>
}

class TripRepositoryImpl(private val dataSource: TripDataSource) : TripRepository {

    override fun getTripList(): Single<List<Trip>> {
        return dataSource.getStoredTripList()
                .doOnEvent { tripList, _ -> if (tripList.isEmpty()) throw NoSuchElementException() }
                .toSingle()
                .onErrorResumeNext {
                    dataSource.fetchTripList()
                            .toObservable()
                            .doOnNext {
                                upsert(it)
                            }
                            .singleOrError()
                }
    }

    override fun getTripDetail(tripId: Int): Single<Trip> {
        return dataSource.getTripDetail(tripId)
    }

    private fun upsert(tripList: List<Trip>) {
        try {
            dataSource.storeTripList(tripList)
        } catch (exception: SQLiteConstraintException) {
            dataSource.updateTripList(tripList)
        }

    }
}