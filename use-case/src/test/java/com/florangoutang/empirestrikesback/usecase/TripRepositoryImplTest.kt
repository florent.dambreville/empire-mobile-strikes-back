package com.florangoutang.empirestrikesback.usecase

import android.database.sqlite.SQLiteConstraintException
import com.florangoutang.empirestrikesback.entity.Trip
import com.florangoutang.empirestrikesback.usecase.trip.TripDataSource
import com.florangoutang.empirestrikesback.usecase.trip.TripRepositoryImpl
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.then
import io.reactivex.Maybe
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.internal.schedulers.TrampolineScheduler
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.functions.Function
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.lang.Exception

@RunWith(MockitoJUnitRunner::class)
class TripRepositoryImplTest {

    @InjectMocks lateinit var repository: TripRepositoryImpl
    @Mock lateinit var dataSource: TripDataSource

    private val mTrampolineSchedulerFunction = Function<Scheduler, Scheduler> { TrampolineScheduler.instance() }

    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler(mTrampolineSchedulerFunction)
        RxJavaPlugins.setComputationSchedulerHandler(mTrampolineSchedulerFunction)
        RxJavaPlugins.setNewThreadSchedulerHandler(mTrampolineSchedulerFunction)
    }

    @Test
    fun `getTripList should call local dataSource`() {
        // Given
        given(dataSource.getStoredTripList()).willReturn(Maybe.just(listOf(mock())))

        // When
       repository.getTripList()

        // Then
        then(dataSource).should().getStoredTripList()
    }

    @Test
    fun `getTripList when dataSource return empty from local should call dataSource from remote`() {
        // Given
        given(dataSource.getStoredTripList()).willReturn(Maybe.empty())
        given(dataSource.fetchTripList()).willReturn(Single.just(mock()))

        // When
        repository.getTripList().test()

        // Then
        then(dataSource).should().getStoredTripList()
        then(dataSource).should().fetchTripList()
    }

    @Test
    fun `getTripList when dataSource return tripList from remote should store this list in dataSource`() {
        // Given
        val trip = mock<Trip>()
        val tripList = listOf(trip)
        given(dataSource.getStoredTripList()).willReturn(Maybe.empty())
        given(dataSource.fetchTripList()).willReturn(Single.just(tripList))

        // When
        val testObserver = repository.getTripList().test()

        // Then
        then(dataSource).should().fetchTripList()
        then(dataSource).should().storeTripList(tripList)
        testObserver.assertValue { it.first() == trip }
        testObserver.assertNoErrors()
    }

    @Test
    fun `getTripList when dataSource return tripList should try to store this list and if already exist should update existing list`() {
        // Given
        val trip = mock<Trip>()
        val tripList = listOf(trip)
        given(dataSource.fetchTripList()).willReturn(Single.just(tripList))
        given(dataSource.getStoredTripList()).willReturn(Maybe.empty())
        given(dataSource.storeTripList(tripList)).willThrow(SQLiteConstraintException("error"))

        // When
        val testObserver = repository.getTripList().test()

        // Then
        then(dataSource).should().fetchTripList()
        then(dataSource).should().storeTripList(tripList)
        then(dataSource).should().updateTripList(tripList)
        testObserver.assertValue { it.first() == trip }
        testObserver.assertNoErrors()
    }

    @Test
    fun `getTripList when dataSource return error should NOT store this list in dataSource`() {
        // Given
        val trip = mock<Trip>()
        val tripList = listOf(trip)
        given(dataSource.getStoredTripList()).willReturn(Maybe.empty())
        given(dataSource.fetchTripList()).willReturn(Single.error(Exception()))

        // When
        val testObserver = repository.getTripList().test()

        // Then
        then(dataSource).should().fetchTripList()
        then(dataSource).should(never()).storeTripList(tripList)
        testObserver.assertNoValues()
        testObserver.assertError(Exception::class.java)
    }

    @After
    fun tearsDown() {
        RxJavaPlugins.reset()
    }
}